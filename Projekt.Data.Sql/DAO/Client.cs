using System;
using System.Collections.Generic;

namespace Projekt.Data.Sql.DAO
{
    public class Client
    {
        public Client()
        {
            ClientOrders = new List<Order>();
        }

        public int ClientId { get; set; }
        public DateTime ClientRegistrationDate { get; set; }
        public string ClientFirstName { get; set; }
        public string ClientLastNAme { get; set; }
        public string ClientAddress { get; set; }
        
        public virtual ICollection<Order> ClientOrders { get; set; }
    }
}