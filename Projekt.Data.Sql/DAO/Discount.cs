using System.Collections.Generic;

namespace Projekt.Data.Sql.DAO
{
    public class Discount
    {
        public Discount()
        {
        }

        public int DiscountId { get; set; }
        public string DiscountCode { get; set; }
        public float DiscountPercentage { get; set; }
        public bool DiscountUsed { get; set; }
    }
}