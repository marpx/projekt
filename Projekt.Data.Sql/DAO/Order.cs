using System;
using System.Collections.Generic;

namespace Projekt.Data.Sql.DAO
{
    public class Order
    {
        public Order()
        {
        }

        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderType { get; set; }
        public string OrderDescription { get; set; }
        public int OrderStatus { get; set; }
        public int WorkerId { get; set; }
        public int ClientId { get; set; }
        
        public virtual Worker Worker { get; set; }
        public virtual Client Client { get; set; }
    }
}