using System.Collections.Generic;

namespace Projekt.Data.Sql.DAO
{
    public class Worker
    {
        public Worker()
        {
            WorkerOrders = new List<Order>();
        }

        public int WorkerId { get; set; }
        public string WorkerFirstName { get; set; }
        public string WorkerLastNAme { get; set; }
        public int WorkerSalary { get; set; }
        
        public virtual ICollection<Order> WorkerOrders { get; set; }
    }
}