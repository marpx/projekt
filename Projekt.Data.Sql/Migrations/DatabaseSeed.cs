using System;
using System.Collections.Generic;
using System.Linq;
using Projekt.Data.Sql.DAO;

namespace Projekt.Data.Sql.Migrations
{
    public class DatabaseSeed
    {
        private readonly ProjektDbContext _context;

        //wstrzyknięcie instancji klasy ProjektDbContext poprzez konstruktor
        public DatabaseSeed(ProjektDbContext context)
        {
            _context = context;
        }
        
        //metoda odpowiadająca za uzupełnienie utworzonej bazy danych testowymi danymi
        //kolejność wywołania ma niestety znaczenie, ponieważ nie da się utworzyć rekordu
        //w bazie dnaych bez znajmości wartości klucza obcego
        //dlatego należy zacząć od uzupełniania tabel, które nie posiadają kluczy obcych
        //--OFFTOP
        //w przeciwną stronę działa ręczne usuwanie tabel z wypełnionymi danymi w bazie danych
        //należy zacząć od tabel, które posiadają klucze obce, a skończyć na tabelach, które 
        //nie posiadają
        public void Seed()
        {
            var clientList = BuildClientsList();
            _context.Client.AddRange(clientList);
            _context.SaveChanges();
            
            var discountList = BuildDiscountList();
            _context.Discount.AddRange(discountList);
            _context.SaveChanges();
            
            var workerList = BuildWorkersList();
            _context.Worker.AddRange(workerList);
            _context.SaveChanges();
            
            var orderList = BuildOrderList(workerList);
            _context.Order.AddRange(orderList);
            _context.SaveChanges();
        }

        private IEnumerable<Client> BuildClientsList()
        {
            var clientList = new List<Client>();
            var client = new Client()
            {
                ClientFirstName = "Jan",
                ClientLastNAme = "Kowalski",
                ClientAddress = "ul. Oleska 16/22, Opole",
            };
            clientList.Add(client);
            var client2 = new Client()
            {
                ClientFirstName = "Zbigniew",
                ClientLastNAme = "Malinowski",
                ClientAddress = "ul. Sosnowskiego 66/15, Opole",
            };
            clientList.Add(client2);
            var client3 = new Client()
            {
                ClientFirstName = "Ryszard",
                ClientLastNAme = "Adamczyk",
                ClientAddress = "ul. Batorego 12/64, Wrocław",
            };
            clientList.Add(client3);
            var client4 = new Client()
            {
                ClientFirstName = "Damian",
                ClientLastNAme = "Dymek",
                ClientAddress = "ul. Jana Pawła II 1/10, Wrocław",
            };
            clientList.Add(client4);
            return clientList;
        }

        private IEnumerable<Discount> BuildDiscountList()
        {
            var discountList = new List<Discount>();
            var discount = new Discount()
            {
                DiscountCode = "0000-1111",
                DiscountPercentage = 5,
                DiscountUsed = true
            };
            discountList.Add(discount);
            var discount2 = new Discount()
            {
                DiscountCode = "2222-3333",
                DiscountPercentage = 2,
                DiscountUsed = false
            };
            discountList.Add(discount2);
            var discount3 = new Discount()
            {
                DiscountCode = "4444-5555",
                DiscountPercentage = 7,
                DiscountUsed = false
            };
            discountList.Add(discount3);
            return discountList;
        }
        private IEnumerable<Worker> BuildWorkersList()
        {
            var workerList = new List<Worker>();
            var worker = new Worker()
            {
                WorkerFirstName = "Mieczysław",
                WorkerLastNAme = "Mieczykowski",
                WorkerSalary= 3100,
            };
            workerList.Add(worker);
            var worker2 = new Worker()
            {
                WorkerFirstName = "Jarosław",
                WorkerLastNAme = "Jarosławski",
                WorkerSalary= 3200,
            };
            workerList.Add(worker2);
            var worker3 = new Worker()
            {
                WorkerFirstName = "Kamil",
                WorkerLastNAme = "Kamilowski",
                WorkerSalary= 3300,
            };
            workerList.Add(worker3);
            return workerList;
        }

        private IEnumerable<Order> BuildOrderList(IEnumerable<Worker> workerList)
        {
            var orderList = new List<Order>();
            var order = new Order()
            {
                OrderDate = DateTime.Now.AddMonths(-2),
                OrderType = "Wymiana matrycy w laptopie",
                OrderStatus = 4,
                WorkerId = 1,
                ClientId = 1,
            };
            orderList.Add(order);
            var order2 = new Order()
            {
                OrderDate = DateTime.Now.AddMonths(-1).AddDays(-6),
                OrderType = "Wymiana dysku w laptopie",
                OrderStatus = 4,
                WorkerId = workerList.First(x=>x.WorkerFirstName=="Mieczysław").WorkerId,
                ClientId = 1,
            };
            orderList.Add(order2);
            var order3 = new Order()
            {
                OrderDate = DateTime.Now.AddDays(-28),
                OrderType = "Reinstalacja systemu operacyjnego",
                OrderStatus = 4,
                WorkerId = 2,
                ClientId = 2,
            };
            orderList.Add(order3);
            var order4 = new Order()
            {
                OrderDate = DateTime.Now.AddDays(-20),
                OrderType = "Wymiana płyty głównej, procesora i pamięci ram",
                OrderStatus = 4,
                WorkerId = 2,
                ClientId = 3,
            };
            orderList.Add(order4);
            var order5 = new Order()
            {
                OrderDate = DateTime.Now.AddDays(-5),
                OrderType = "Wymiana płyty głównej w laptopie",
                OrderStatus = 2,
                WorkerId = 2,
                ClientId = 1,
            };
            orderList.Add(order5);
            return orderList;
        }
    }
}