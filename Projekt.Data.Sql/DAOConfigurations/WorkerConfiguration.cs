using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Projekt.Data.Sql.DAO;

namespace Projekt.Data.Sql.DAOConfigurations
{
    public class WorkerConfiguration : IEntityTypeConfiguration<Worker>
    {
        public void Configure(EntityTypeBuilder<Worker> builder)
        {
            builder.Property(c => c.WorkerFirstName).IsRequired();
            builder.Property(c => c.WorkerLastNAme).IsRequired();
            builder.ToTable("Worker");
        }
        
    }
}