using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Projekt.Data.Sql.DAO;

namespace Projekt.Data.Sql.DAOConfigurations
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.Property(c => c.OrderDate).IsRequired();
            builder.Property(c => c.OrderType).IsRequired();
            builder.HasOne(x => x.Client)
                .WithMany(x => x.ClientOrders)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.ClientId);
            builder.HasOne(x => x.Worker)
                .WithMany(x => x.WorkerOrders)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.WorkerId);
            builder.ToTable("Order");
        }
        
    }
}