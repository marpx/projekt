using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Projekt.Data.Sql.DAO;

namespace Projekt.Data.Sql.DAOConfigurations
{
    public class DiscountConfiguration : IEntityTypeConfiguration<Discount>
    {
        public void Configure(EntityTypeBuilder<Discount> builder)
        {
            builder.Property(c => c.DiscountCode).IsRequired();
            builder.Property(c => c.DiscountPercentage).IsRequired();
            builder.Property(c => c.DiscountUsed).IsRequired();
            builder.Property(c => c.DiscountUsed).HasColumnType("tinyint(1)");
            builder.ToTable("Discount");
        }
        
    }
}