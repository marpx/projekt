using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Projekt.Data.Sql.DAO;

namespace Projekt.Data.Sql.DAOConfigurations
{
    public class ClientConfiguration : IEntityTypeConfiguration<Client>
    {
        public void Configure(EntityTypeBuilder<Client> builder)
        {
            builder.Property(c => c.ClientFirstName).IsRequired();
            builder.Property(c => c.ClientLastNAme).IsRequired();
            builder.ToTable("Client");
        }
        
    }
}