using Projekt.Data.Sql.DAO;
using Projekt.Data.Sql.DAOConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Projekt.Data.Sql
{
    public class ProjektDbContext : DbContext
    {
        public ProjektDbContext(DbContextOptions<ProjektDbContext> options) : base(options) {}

        //Ustawienie klas z folderu DAO jako tabele bazy danych

        public virtual DbSet<Client> Client { get; set; }        

        public virtual DbSet<Discount> Discount { get; set; }        

        public virtual DbSet<Order> Order { get; set; }        

        public virtual DbSet<Worker> Worker { get; set; }

        //Przykład konfiguracji modeli/encji poprzez klasy konfiguracyjne z folderu DAOConfigurations

        protected override void OnModelCreating(ModelBuilder builder)
        {

            builder.ApplyConfiguration(new ClientConfiguration());

            builder.ApplyConfiguration(new DiscountConfiguration());

            builder.ApplyConfiguration(new OrderConfiguration());

            builder.ApplyConfiguration(new WorkerConfiguration());

        }
    }
}